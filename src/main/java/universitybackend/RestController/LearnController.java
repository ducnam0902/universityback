package universitybackend.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import universitybackend.Services.LearnServices;
import universitybackend.model.Learn;
import universitybackend.model.Scoring;

@RestController
@RequestMapping(path="learn", produces = "application/json")
@CrossOrigin(origins = "*")
public class LearnController {
	private LearnServices learnServices;
	public LearnController(LearnServices learnServices) {
		this.learnServices=learnServices;
	}
	@GetMapping(value="/student/{id}")
	public List<Learn> showResultStudent(@PathVariable("id") String id){
		return learnServices.findByStudentId(id);
	}
	@GetMapping(value="/stuandsub/{id1}/{id2}")
	public Learn findByStudentandSubject(@PathVariable("id1") String id1,@PathVariable("id2") String id2) {
		return learnServices.findByStudentIdandSubjectId(id1, id2);
	}
	@PostMapping
	public void addNewLearn(@RequestBody Learn learn) {
		learnServices.save(learn);
	}
	@GetMapping( value="/studentId/{id}")
	public List<Scoring> getScoringbyStudentId(@PathVariable("id") String id){
		return learnServices.findScoringByStudentId(id);
	}
	
	
}

package universitybackend.RestController;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import universitybackend.Services.SubjectServices;
import universitybackend.model.ProgramSubject;
import universitybackend.model.SubjectId;


@RestController
@RequestMapping(path="subject", produces = "application/json")
@CrossOrigin(origins = "*")
public class SubjectController {
	private SubjectServices subjectServices;
	public SubjectController(SubjectServices subjectServices){
	    this.subjectServices=subjectServices;
	}
	@GetMapping
	public List<ProgramSubject> getAllSubject(){
	    return subjectServices.findAll();
	}
	@GetMapping(value="edit/{id1}/{id2}")
	public Optional<ProgramSubject> getaSubject(@PathVariable("id1")String id1,@PathVariable("id2")String id2) {
		SubjectId sub= new SubjectId(id1,id2);
		return  subjectServices.findById(sub);
	}
	@PostMapping
	public void addNewSubject(@RequestBody ProgramSubject subject) {
		subjectServices.save(subject);
	}
	@DeleteMapping(value="/delete/{id1}/{id2}")
	public void deleteSubject(@PathVariable("id1")String id1,@PathVariable("id2")String id2) {
		SubjectId sub= new SubjectId(id1,id2);
		subjectServices.remove(sub);
	}
	@GetMapping(value="/count")
	public String countSubject() {
		return subjectServices.countSubject();
	}
	@GetMapping(value="/subjectName/{id}")
	public String getSubjectName(@PathVariable("id") String id) {
		return subjectServices.getSubjectName(id);
	}
	@GetMapping(value="/subjectId/{id}")
	public String getSubjectId(@PathVariable("id") String id) {
		return subjectServices.getSubjectId(id);
	}
	@GetMapping(value="/allSubjectName")
	public List<String> getAllSubjectName(){
		return subjectServices.getAllSubjectName();
	}
	@GetMapping(value="/programId/{id}")
	public String getProgramId(@PathVariable("id") String id){
		return subjectServices.getProgramId(id);
	}
}

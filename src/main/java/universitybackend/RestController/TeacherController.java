package universitybackend.RestController;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import universitybackend.Services.TeacherServices;
import universitybackend.model.Teacher;
@RestController
@RequestMapping(path="teacher", produces = "application/json")
@CrossOrigin(origins = "*")
public class TeacherController {
	private TeacherServices teaServices;
	public TeacherController(TeacherServices teaServices) {
		this.teaServices=teaServices;
	}
	@GetMapping
	public List<Teacher> getAllTeacher(){
	    return teaServices.findAll();
	}
	@GetMapping(value="edit/{id}")
	public Optional<Teacher> getaTeacher(@PathVariable("id")String id) {
		return  teaServices.findById(id);
	}
	@PostMapping
	public void addNewTeacher(@RequestBody Teacher teacher) {
		teaServices.save(teacher);
	}
	@DeleteMapping(value="/delete/{id}")
	public void deleteTeacher(@PathVariable("id")String id) {
		teaServices.remove(id);
		
	}
	@GetMapping(value="/count")
	public String countTeacher() {
		return teaServices.countTeacher();
	}
	@GetMapping(value="/listTeacherId")
	public List<String> listIdTeacher(){
		return teaServices.listIdTeacher();
	}
	
}

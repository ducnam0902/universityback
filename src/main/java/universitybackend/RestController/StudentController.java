package universitybackend.RestController;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import universitybackend.Services.StudentServices;
import universitybackend.model.Student;

@RestController
@RequestMapping(path="student", produces = "application/json")
@CrossOrigin(origins = "*")
public class StudentController {
	private StudentServices stuServices;
	public StudentController(StudentServices stuServices){
	    this.stuServices=stuServices;
	}
	@GetMapping
	public List<Student> getAllStudent(){
	    return stuServices.findAll();
	}
	@GetMapping(value="edit/{id}")
	public Optional<Student> getaStudent(@PathVariable("id")String id) {
		return  stuServices.findById(id);
	}
	@PostMapping
	public void addNewStudent(@RequestBody Student student) {
		stuServices.save(student);
	}
	@DeleteMapping(value="/delete/{id}")
	public void deleteStudent(@PathVariable("id")String id) {
		stuServices.remove(id);
	}
	@GetMapping(value="/count")
	public String countStudent() {
		return stuServices.countStudent();
	}
}

package universitybackend.RestController;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import universitybackend.Services.FalcutyServices;
import universitybackend.model.Falcuty;

@RestController
@RequestMapping(path="falcuty", produces = "application/json")
@CrossOrigin(origins = "*")
public class FalcutyController {

	private FalcutyServices falcutyServices;
	public FalcutyController(FalcutyServices falcutyServices){
	    this.falcutyServices=falcutyServices;
	}
	@GetMapping
	public List<Falcuty> getAllKhoa(){
	    return falcutyServices.findAll();
	}
	@GetMapping(value="edit/{id}")
	public Optional<Falcuty> getaFalcuty(@PathVariable("id")String id) {
		return  falcutyServices.findById(id);
	}
	@PostMapping
	public void addNewFalculty(@RequestBody Falcuty falcuty) {
		falcutyServices.save(falcuty);
	}
	@DeleteMapping(value="/delete/{id}")
	public void deleteFalcuty(@PathVariable("id")String id) {
		falcutyServices.remove(id);
	}
	@GetMapping(value="/count")
	public String countFalcuty() {
		return falcutyServices.countFalcuty();
	}
	@GetMapping(value="/name")
	public List<String> falcutyName() {
		return falcutyServices.falcutyName();
	}
}

package universitybackend.RestController;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import universitybackend.Services.EducationProgramServices;
import universitybackend.model.EducationProgram;

@RestController
@RequestMapping(path="edu", produces = "application/json")
@CrossOrigin(origins = "*")
public class EducationProgramController {
	private EducationProgramServices eduServices;
	public EducationProgramController(EducationProgramServices eduServices){
	    this.eduServices=eduServices;
	}
	@GetMapping
	public List<EducationProgram> getAllEducationProgram(){
	    return eduServices.findAll();
	}
	@GetMapping(value="edit/{id}")
	public Optional<EducationProgram> getaEducationProgram(@PathVariable("id")String id) {
		return  eduServices.findById(id);
	}
	@PostMapping
	public void addNewEducationProgram(@RequestBody EducationProgram EducationProgram) {
		eduServices.save(EducationProgram);
	}
	@DeleteMapping(value="/delete/{id}")
	public void deleteEducationProgram(@PathVariable("id")String id) {
		eduServices.remove(id);
	}
	@GetMapping(value="/count")
	public String countEdu() {
		return eduServices.countEducation();
	}
	@GetMapping(value="/listid")
	public List<String> showListId(){
		return eduServices.listFalcutyId();
	}
	@GetMapping(value="/falcutyName/{id}")
	public String falcutyName(@PathVariable("id")String id) {
		return eduServices.falcutyName(id);
	}
	@GetMapping(value="/listProgramId")
	public List<String> listProgramId(){
		return eduServices.listProgramId();
	}
	
}

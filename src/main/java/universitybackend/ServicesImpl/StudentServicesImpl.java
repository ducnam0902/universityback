package universitybackend.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import universitybackend.JPARepository.StudentRepoitory;
import universitybackend.Services.StudentServices;
import universitybackend.model.Student;

@Service
public class StudentServicesImpl implements StudentServices{
	@Autowired
	StudentRepoitory stuRepo;
	@Override
	public List<Student> findAll() {
		// TODO Auto-generated method stub
		return stuRepo.findAll();
	}

	@Override
	public void save(Student f) {
		stuRepo.save(f);
		
	}

	@Override
	public void remove(String f) {
		stuRepo.deleteById(f);
		
	}

	@Override
	public Optional<Student> findById(String id) {
		return stuRepo.findById(id);
	}

	@Override
	public String countStudent() {
		// TODO Auto-generated method stub
		return stuRepo.countStudent();
	}

}

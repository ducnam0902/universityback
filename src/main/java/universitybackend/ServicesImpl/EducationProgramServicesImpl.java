package universitybackend.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import universitybackend.JPARepository.EducationProgramRepository;
import universitybackend.JPARepository.FalcutyRepository;
import universitybackend.Services.EducationProgramServices;
import universitybackend.model.EducationProgram;

@Service
public class EducationProgramServicesImpl implements EducationProgramServices {

	@Autowired
	EducationProgramRepository eduRepo;
	@Autowired
	FalcutyRepository falRepo;
	@Override
	public List<EducationProgram> findAll() {
		// TODO Auto-generated method stub
		return eduRepo.findAll();
	}
	@Override
	public void save(EducationProgram f) {
		eduRepo.save(f);
	}
	@Override
	public void remove(String f) {
		eduRepo.deleteById(f);
	}
	@Override
	public Optional<EducationProgram> findById(String id) {
		return eduRepo.findById(id);
	}
	@Override
	public String countEducation() {
		// TODO Auto-generated method stub
		return eduRepo.countEducation();
	}
	@Override
	public List<String> listFalcutyId() {
		return falRepo.listIdFalcuty();
	}
	@Override
	public String falcutyName(String id) {
		// TODO Auto-generated method stub
		return falRepo.falcutyName(id);
	}
	@Override
	public List<String> listProgramId() {
		// TODO Auto-generated method stub
		return eduRepo.showListIdEducation();
	}
}

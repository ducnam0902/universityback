package universitybackend.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import universitybackend.JPARepository.TeacherRepoitory;
import universitybackend.Services.TeacherServices;
import universitybackend.model.Teacher;

@Service
public class TeacherServicesImpl implements TeacherServices{
	@Autowired
	TeacherRepoitory teacherRepo;
	@Override
	public List<Teacher> findAll() {
		return teacherRepo.findAll();
	}
	@Override
	public void save(Teacher f) {
		teacherRepo.save(f);
	}
	@Override
	public void remove(String f) {
		teacherRepo.deleteById(f);
	}
	@Override
	public Optional<Teacher> findById(String id) {
		return teacherRepo.findById(id);
	}
	@Override
	public String countTeacher() {
		return teacherRepo.countTeacher();
	}
	@Override
	public List<String> listIdTeacher() {
		return teacherRepo.showListIdTeacher();
	}

}

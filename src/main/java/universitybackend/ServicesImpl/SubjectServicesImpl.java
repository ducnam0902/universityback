package universitybackend.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import universitybackend.JPARepository.SubjectRepository;
import universitybackend.Services.SubjectServices;
import universitybackend.model.ProgramSubject;
import universitybackend.model.SubjectId;

@Service
public class SubjectServicesImpl implements SubjectServices {

	@Autowired
	SubjectRepository subRepo;
	@Override
	public List<ProgramSubject> findAll() {
		return subRepo.findAll();
	}
	@Override
	public void save(ProgramSubject f) {
		subRepo.save(f);
	}
	@Override
	public void remove(SubjectId f) {
		subRepo.deleteById(f);
		
	}
	@Override
	public Optional<ProgramSubject> findById(SubjectId id) {
		return subRepo.findById(id);
	}
	@Override
	public String countSubject() {
		return subRepo.countSubject();
	}
	@Override
	public String getSubjectName(String id) {
		// TODO Auto-generated method stub
		return subRepo.getSubjectName(id);
	}
	@Override
	public List<String> getAllSubjectName() {
		// TODO Auto-generated method stub
		return subRepo.getAllsubjectName();
	}
	@Override
	public String getSubjectId(String id) {
		// TODO Auto-generated method stub
		return subRepo.getSubjectId(id);
	}
	@Override
	public String getProgramId(String id) {
		// TODO Auto-generated method stub
		return subRepo.getProgramId(id);
	}

}

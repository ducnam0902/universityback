package universitybackend.ServicesImpl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import universitybackend.JPARepository.LearnRepository;
import universitybackend.Services.LearnServices;
import universitybackend.model.Learn;
import universitybackend.model.Scoring;

@Service
public class LearnServicesImpl implements LearnServices {
	@Autowired
	LearnRepository learnRepo;
	@Override
	public void save(Learn l) {
		learnRepo.save(l);
	}
	@Override
	public List<Learn> findByStudentId(String studentId) {
		// TODO Auto-generated method stub
		return learnRepo.findByStudentId(studentId);
	}
	@Override
	public Learn findByStudentIdandSubjectId(String studentId, String subjectId) {
		
		return learnRepo.findByStudentIdAndSubjectId(studentId, subjectId);
	}
	@Override
	public List<Scoring> findScoringByStudentId(String id) {
		// TODO Auto-generated method stub
		return learnRepo.findScoringByStudent(id);
	}
}

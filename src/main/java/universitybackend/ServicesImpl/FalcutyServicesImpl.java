package universitybackend.ServicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import universitybackend.JPARepository.FalcutyRepository;
import universitybackend.Services.FalcutyServices;
import universitybackend.model.Falcuty;

@Service
public class FalcutyServicesImpl implements FalcutyServices {

	@Autowired
	FalcutyRepository FalcutyRepo;
	@Override
	public List<Falcuty> findAll() {
		return FalcutyRepo.findAll();
	}
	@Override
	public void save(Falcuty f) {
		FalcutyRepo.save(f);
	}
	@Override
	public void remove(String f) {
		FalcutyRepo.deleteById(f);
	}
	@Override
	public Optional<Falcuty> findById(String id) {
		return FalcutyRepo.findById(id);
	}
	@Override
	public String countFalcuty() {
		// TODO Auto-generated method stub
		return FalcutyRepo.countFalcuty();
	}
	@Override
	public List<String> falcutyName() {
		return FalcutyRepo.falcutyName();
	}

}

package universitybackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversitybackApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniversitybackApplication.class, args);
	}

}

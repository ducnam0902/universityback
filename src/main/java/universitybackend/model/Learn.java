package universitybackend.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(LearnId.class)
public class Learn {
	@Id
	String studentId;
	@Id
	String subjectId;
	@Id
	String programId;
	String score_1;
	String score_2;
	String score_3;
	String mid_score;
	public Learn() {
		super();
	}
	public Learn(String studentId, String subjectId, String programId, String score_1, String score_2, String score_3,
			String mid_score) {
		super();
		this.studentId = studentId;
		this.subjectId = subjectId;
		this.programId = programId;
		this.score_1 = score_1;
		this.score_2 = score_2;
		this.score_3 = score_3;
		this.mid_score = mid_score;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getScore_1() {
		return score_1;
	}
	public void setScore_1(String score_1) {
		this.score_1 = score_1;
	}
	public String getScore_2() {
		return score_2;
	}
	public void setScore_2(String score_2) {
		this.score_2 = score_2;
	}
	public String getScore_3() {
		return score_3;
	}
	public void setScore_3(String score_3) {
		this.score_3 = score_3;
	}
	public String getMid_score() {
		return mid_score;
	}
	public void setMid_score(String mid_score) {
		this.mid_score = mid_score;
	}
		
}

package universitybackend.model;

public class Scoring {
	String subjectId;
	String subjectName;
	String score_1;
	String score_2;
	String score_3;
	String midScore;
	public Scoring(String subjectId, String subjectName, String score_1, String score_2, String score_3,
			String midScore) {
		super();
		this.subjectId = subjectId;
		this.subjectName = subjectName;
		this.score_1 = score_1;
		this.score_2 = score_2;
		this.score_3 = score_3;
		this.midScore = midScore;
	}
	public Scoring() {
		super();
	}
	
}

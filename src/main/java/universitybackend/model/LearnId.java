package universitybackend.model;

import java.io.Serializable;

public class LearnId implements Serializable {
	String studentId;
	String subjectId;
	String programId;
	public LearnId() {
		super();
	}
	public LearnId(String studentId, String subjectId, String programId) {
		super();
		this.studentId = studentId;
		this.subjectId = subjectId;
		this.programId = programId;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	
}

package universitybackend.model;

import java.io.Serializable;

public class SubjectId implements Serializable{
	String subjectId;
	String programId;
	public SubjectId(String subjectId, String programId) {
		super();
		this.subjectId = subjectId;
		this.programId = programId;
	}
	public SubjectId() {
		super();
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	
}

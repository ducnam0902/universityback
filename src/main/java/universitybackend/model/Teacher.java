package universitybackend.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Teacher {

	@Id
	String teacherId;
	String teacherName;
	int seniority;
	
	public Teacher() {
		super();
	}
	public Teacher(String teacherId, String teacherName, int seniority) {
		super();
		this.teacherId = teacherId;
		this.teacherName = teacherName;
		this.seniority = seniority;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public int getSeniority() {
		return seniority;
	}
	public void setSeniority(int seniority) {
		this.seniority = seniority;
	}
	
}

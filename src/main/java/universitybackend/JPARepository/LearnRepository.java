package universitybackend.JPARepository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import universitybackend.model.Learn;
import universitybackend.model.Scoring;

@Repository
public interface LearnRepository extends JpaRepository<Learn,Object>{
	List<Learn> findByStudentId(String studentId);
	Learn findByStudentIdAndSubjectId(String studentId,String subjectId);
	@Query(value="SELECT LEARN.SUBJECT_ID,SUBJECT_NAME,SCORE_1,SCORE_2,SCORE_3,MID_SCORE FROM LEARN,PROGRAM_SUBJECT WHERE LEARN.SUBJECT_ID = PROGRAM_SUBJECT.SUBJECT_ID AND STUDENT_ID=?1",nativeQuery= true)
	public List<Scoring> findScoringByStudent(String id);
}

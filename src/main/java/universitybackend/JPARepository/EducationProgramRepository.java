package universitybackend.JPARepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import universitybackend.model.Education;
import universitybackend.model.EducationProgram;

@Repository
public interface EducationProgramRepository extends JpaRepository<EducationProgram,String>{
	@Query(value = "SELECT count(*) FROM education_program", nativeQuery = true)
	public String countEducation();
	@Query(value = "SELECT * FROM education_program",nativeQuery = true)
	public List<Education> showListEducation();
	@Query(value = "SELECT program_id FROM education_program",nativeQuery= true)
	public List<String> showListIdEducation();
	
}

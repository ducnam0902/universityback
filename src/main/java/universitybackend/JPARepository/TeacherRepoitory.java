package universitybackend.JPARepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import universitybackend.model.Teacher;


@Repository
public interface TeacherRepoitory extends JpaRepository<Teacher,String> {
	@Query(value = "SELECT count(*) FROM Teacher", nativeQuery = true)
	public String  countTeacher();
	@Query(value = "SELECT teacher_id FROM Teacher",nativeQuery= true)
	public List<String> showListIdTeacher();
}

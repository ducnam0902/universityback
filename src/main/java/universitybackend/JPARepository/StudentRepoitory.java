package universitybackend.JPARepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import universitybackend.model.Student;

@Repository
public interface StudentRepoitory extends JpaRepository<Student,String> {
	@Query(value = "SELECT count(*) FROM Student", nativeQuery = true)
	public String  countStudent();
	
}

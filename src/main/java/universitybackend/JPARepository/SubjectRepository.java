package universitybackend.JPARepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import universitybackend.model.ProgramSubject;


@Repository
public interface SubjectRepository extends JpaRepository<ProgramSubject,Object> {
	@Query(value = "SELECT count(*) FROM Program_Subject", nativeQuery = true)
	public String  countSubject();
	@Query(value ="SELECT subject_name FROM program_subject where subject_id=?1",nativeQuery = true)
	public String getSubjectName(String id);
	@Query(value = "SELECT subject_name from program_subject",nativeQuery = true)
	public List<String> getAllsubjectName();
	@Query(value ="SELECT subject_id FROM program_subject where subject_name=?1",nativeQuery = true)
	public String getSubjectId(String id);
	@Query(value ="SELECT program_id FROM program_subject where subject_name=?1",nativeQuery = true)
	public String getProgramId(String id);
}

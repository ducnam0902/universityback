package universitybackend.JPARepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import universitybackend.model.Falcuty;
@Repository
public interface FalcutyRepository extends JpaRepository<Falcuty,String>  {
	@Query(value = "SELECT count(*) FROM Falcuty", nativeQuery = true)
	public String  countFalcuty();
	@Query(value = "SELECT falcuty_id FROM Falcuty", nativeQuery = true)
	public List<String> listIdFalcuty();
	@Query(value="SELECT falcuty_name from Falcuty where falcuty_id=?1", nativeQuery = true)
	public String falcutyName(String id);
	@Query(value="SELECT falcuty_name from Falcuty",nativeQuery= true)
	public List<String> falcutyName();
}

package universitybackend.Services;

import java.util.List;
import java.util.Optional;
import universitybackend.model.ProgramSubject;
import universitybackend.model.SubjectId;
public interface SubjectServices {
	List<ProgramSubject> findAll();
	public void save(ProgramSubject f);
	public void remove(SubjectId f);
	public Optional<ProgramSubject> findById(SubjectId id);
	public String countSubject();
	public String getSubjectName(String id);
	public List<String> getAllSubjectName();
	public String getSubjectId(String id);
	public String getProgramId(String id);
}

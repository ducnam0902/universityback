package universitybackend.Services;

import java.util.List;
import java.util.Optional;

import universitybackend.model.Falcuty;

public interface FalcutyServices {
	List<Falcuty> findAll();
	public void save(Falcuty f);
	public void remove(String f);
	public Optional<Falcuty> findById(String id);
	public String countFalcuty();
	public List<String> falcutyName();
}

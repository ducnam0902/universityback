package universitybackend.Services;

import java.util.List;
import java.util.Optional;

import universitybackend.model.EducationProgram;

public interface EducationProgramServices {
	List<EducationProgram> findAll();
	public void save(EducationProgram f);
	public void remove(String f);
	public Optional<EducationProgram> findById(String id);
	public String countEducation();
	public List<String> listFalcutyId();
	public String falcutyName(String id);
	public List<String> listProgramId();
}

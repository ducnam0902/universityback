package universitybackend.Services;

import java.util.List;
import java.util.Optional;

import universitybackend.model.Student;

public interface StudentServices {
	List<Student> findAll();
	public void save(Student f);
	public void remove(String f);
	public Optional<Student> findById(String id);
	public String countStudent();
}

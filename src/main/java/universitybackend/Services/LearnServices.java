package universitybackend.Services;

import java.util.List;

import universitybackend.model.Learn;
import universitybackend.model.Scoring;

public interface LearnServices {
	public void save(Learn l);
	List<Learn> findByStudentId(String studentId);
	Learn findByStudentIdandSubjectId(String studentId,String subjectId);
	List<Scoring> findScoringByStudentId(String id);
}

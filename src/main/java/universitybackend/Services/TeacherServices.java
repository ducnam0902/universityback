package universitybackend.Services;

import java.util.List;
import java.util.Optional;

import universitybackend.model.Teacher;

public interface TeacherServices {
	List<Teacher> findAll();
	public void save(Teacher f);
	public void remove(String f);
	public Optional<Teacher> findById(String id);
	public String countTeacher();
	public List<String> listIdTeacher();
}
